# Maintainer: Alfonso Commer <acommn@gmail.com>

# Description: Custom GNU/Linux OS kernel compilation, designed to
# make use of compilation time optimizations (flags like
# '-march=native' or other architectures).

_version=5
_patch=4
_subversion=28
_extraversion=0

pkgname=('linux-lts-custom' 'linux-lts-custom-headers' 'linux-lts-custom-docs')
pkgver=${_version}.${_patch}.${_subversion}
pkgrel=1
epoch=
pkgdesc=
arch=('x86_64')
url="https://www.kernel.org"
license=('GPL2')
groups=()
depends=()
makedepends=('gcc' 'make' 'kmod' 'findutils' 'coreutils')
checkdepends=()
optdepends=()
provides=()
conflicts=()
replaces=()
backup=()
options=()
install=
changelog=
source=("https://cdn.kernel.org/pub/linux/kernel/v$_version.x/linux-$pkgver.tar.xz"
        "https://cdn.kernel.org/pub/linux/kernel/v$_version.x/linux-$pkgver.tar.sign"
        "config")
noextract=()
sha256sums=('c863cc1346348f9a40083b4bc0d34375117b1c401af920994d42e855653ef7a4'
            'SKIP'
            '022cb8b754c83a46a39e2b649d2471e6586dfb3af99df143c0340aad1e4f2a77')
# Below this paragraph are the public PGP key fingerprints for Linus
# Torvalds and Greg Kroah-Hartman, these are needed to verify the
# authenticity of the source files.
validpgpkeys=('647F28654894E3BD457199BE38DBBDC86092693E'
              'ABAF11C65A2970B130ABE3C479BE3E4300411886')
# Remember to import them to your keyring with a command like:
#
# gpg --locate-keys torvalds@kernel.org gregkh@kernel.org
#
# OR
#
# gpg --receive-key '647F28654894E3BD457199BE38DBBDC86092693E' 'ABAF11C65A2970B130ABE3C479BE3E4300411886'
#
# If the keys have changed (signatures, ID's, etc) please refresh them
# if they are already in your keyring.
#
# gpg --refresh-keys '647F28654894E3BD457199BE38DBBDC86092693E' 'ABAF11C65A2970B130ABE3C479BE3E4300411886'
#
# Otherwise, the final verification will fail.

prepare() {
  cd "$srcdir/linux-$pkgver";
  # Declare global and exportable variables, needed by several
  # functions.
  declare -gx _kconfig="KCONFIG_OVERWRITECONFIG=true KCONFIG_CONFIG=$srcdir/config";
  declare -gx _kernel_release;
  declare -gx _image_name;
  # If you are testing out this package construction script, don't
  # forget to comment out (# make) or disable (make -n) the 'make
  # cleaning' targets in order to avoid losing all the compiled files.
  make distclean;
  # Update kernel configuration file for this release.
  make $_kconfig oldconfig nconfig;
  # Initialize these variables.
  _kernel_release=$(make --silent $_kconfig kernelrelease);
  _image_name=$(make --silent $_kconfig image_name);
  # Show the variables content.
  echo "Kconfig variables: $_kconfig";
  echo "Kernel release: $_kernel_release";
  echo "Kernel image name: $_image_name";
}

build() {
  cd "$srcdir/linux-$pkgver";
  # The actual build operation. Remember to change the '_arch'
  # variable accordingly for your architecture (see gcc's manual page,
  # at the 'Machine-Dependent Options' and then architecture
  # subsection [in this case, it is 'x86 Options'])
  local _arch=x86-64
  # local _arch=bdver4            # Optimize for AMD Family 15h (Bristol Ridge?) core based CPUs.
  time make --jobs=$(nproc) --output-sync=target --silent $_kconfig CFLAGS_KERNEL="-march=$_arch" CFLAGS_MODULE="-march=$_arch" bzImage modules;
}

package_linux-lts-custom() {
  pkgdesc="A custom build of the GNU/Linux OS kernel and modules"
  arch=('x86_64')
  depends=('coreutils' 'linux-firmware' 'kmod' 'mkinitcpio')
  optdepends=('crda: to set the correct wireless channels of your country')
  groups=('base')
  provides=('linux-lts')
  backup=("etc/mkinitcpio.d/$pkgbase.preset")
  options=('!strip')
  install=$pkgbase.install
  
  cd "$srcdir/linux-$pkgver";
  # Declare local variables.
  local _extramodules_dir="extramodules-$_version.$_patch${pkgbase#linux}";
  # Install modules.
  make $_kconfig INSTALL_MOD_PATH="$pkgdir/usr" modules_install;
  # Make extramodules directory to install out-of-tree modules
  # compiled by the user.
  install --owner=root --group=root --mode=0755 --directory "$pkgdir/usr/lib/modules/$_extramodules_dir";
  ln -s "../$_extramodules_dir" "$pkgdir/usr/lib/modules/$_kernel_release/extramodules";
  # Install a version file inside the extramodules directory through a
  # heredoc.
  install --owner=root --group=root --mode=0644 -D /dev/stdin "$pkgdir/usr/lib/modules/$_extramodules_dir/version" <<EOF;
$_kernel_release
EOF
  # Remove the symlinks pointing to build output and source code
  # directories.
  rm "$pkgdir/usr/lib/modules/$_kernel_release/build";
  rm "$pkgdir/usr/lib/modules/$_kernel_release/source";
  # Generate symbol exports and requires map.
  depmod -b "$pkgdir/usr" -F System.map "$_kernel_release";
  # Install big zipped linux image (bzImage).
  install --owner=root --group=root --mode=0755 -D "$_image_name" "$pkgdir/boot/vmlinuz-$pkgbase";
  # Create a symlink to the image, needed by systemd in order to successfully hibernate.
  # https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
  ln -s "/boot/vmlinuz-$pkgbase" "$pkgdir/usr/lib/modules/$_kernel_release/vmlinuz";
  # Install raw linux image for debugging.
  install --owner=root --group=root --mode=0644 -D "vmlinux" "$pkgdir/usr/lib/modules/$_kernel_release/build/vmlinux";
  # Install mkinitcpio preset file through a heredoc.
  install --owner=root --group=root --mode=0644 -D /dev/stdin "$pkgdir/etc/mkinitcpio.d/$pkgbase.preset" <<EOF;
# mkinitcpio preset file for the "$pkgbase" package.

ALL_config="/etc/mkinitcpio.conf"
ALL_kver="/boot/vmlinuz-$pkgbase"

PRESETS=('default' 'fallback')

#default_config="/etc/mkinitcpio.conf"
default_image="/boot/initramfs-$pkgbase.img"
#default_options=""

#fallback_config="/etc/mkinitcpio.conf"
fallback_image="/boot/initramfs-$pkgbase-fallback.img"
fallback_options="-S autodetect"
EOF
  # Install pacman hooks through a heredoc.
  install --owner=root --group=root --mode=0644 -D /dev/stdin "$pkgdir/usr/share/libalpm/hooks/60-$pkgbase.hook" <<EOF;
[Trigger]
Type = File
Operation = Install
Operation = Upgrade
Operation = Remove
Target = usr/lib/modules/$_kernel_release/*
Target = usr/lib/modules/$_extramodules_dir/*

[Action]
Description = Updating $pkgbase module dependencies...
When = PostTransaction
Exec = /usr/bin/depmod $_kernel_release
EOF
  install --owner=root --group=root --mode=0644 -D /dev/stdin "$pkgdir/usr/share/libalpm/hooks/90-$pkgbase.hook" <<EOF;
[Trigger]
Type = File
Operation = Install
Operation = Upgrade
Target = boot/vmlinuz-$pkgbase
Target = usr/lib/initcpio/*

[Action]
Description = Updating $pkgbase initcpios...
When = PostTransaction
Exec = /usr/bin/mkinitcpio -p $pkgbase
EOF
}

package_linux-lts-custom-headers() {
  pkgdesc="Header files and scripts for building modules for the GNU/Linux OS kernel"
  depends=("linux-lts-custom=$pkgver")
  arch=('x86_64')
  provides=('linux-lts-headers')
  options=('!strip')
  
  cd "$srcdir/linux-$pkgver";
  # Install system map and create base directory structure.
  install --owner=root --group=root --mode=0644 -D "System.map" "$pkgdir/usr/lib/modules/$_kernel_release/build/System.map";
  # Remove most generated files but keep enough build support to build
  # external modules.
  make $_kconfig clean;
  # Copy the entire source tree to the build directory.
  cp -rPT "$srcdir/linux-$pkgver" "$pkgdir/usr/lib/modules/$_kernel_release/build";
  # Remove unneeded architectures for this package target (x86).
  find "$pkgdir/usr/lib/modules/$_kernel_release/build/arch" -mindepth 1 -maxdepth 1 \! \( -name "x86" -o -name "x86_64" \) -type d -printf "Removing arch: %P\n" -execdir rm -r {} \; ;
  # Remove broken symlinks.
  find -L "$pkgdir/usr/lib/modules/$_kernel_release/build/" -type l -printf "Removing broken symlink: %P\n" -delete;
  # Install the kernel configuration file.
  install --owner=root --group=root --mode=0644 -D "$srcdir/config" "$pkgdir/usr/lib/modules/$_kernel_release/build/.config";
  # Remove the documentation folder for this package, the package
  # 'linux-lts-custom-docs' installs this.
  rm -r "$pkgdir/usr/lib/modules/$_kernel_release/build/Documentation";
}

package_linux-lts-custom-docs() {
  pkgdesc="HTML documentation for the GNU/Linux OS kernel"
  depends=("linux-lts-custom=$pkgver")
  arch=('any')
  provides=('linux-lts-docs')
  
  cd "$srcdir/linux-$pkgver";
  # Create base directory.
  install --owner=root --group=root --mode=0755 --directory "$pkgdir/usr/lib/modules/$_kernel_release/build";
  # Copy documentation.
  cp -rP "Documentation" "$pkgdir/usr/lib/modules/$_kernel_release/build/";
}

# vim:set ts=2 sw=2 et:
